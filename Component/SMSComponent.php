<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   SMS
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\Network\Http\Client;
use Cake\Core\Configure;

/**
 * SMS Component
 *
 * @category Component
 * @package  SMS
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.actonate.com/
 */

class SMSComponent extends Component
{
    public $components = ['Special'];

    /**
     *  Initialization
     *    DATE: 24th April 2017
     *
     * @return array
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function __construct()
    {
        $sms_config = Configure::read('sms');

        //$this->username = env('SMS_USERNAME');
        $this->password = $sms_config['MSG91_SMS_PASSWORD'];
        $this->senderID = $sms_config['MSG91_SMS_SENDERID'];
        $this->route = $sms_config['MSG91_SMS_ROUTE'];
        $this->url = $sms_config['MSG91_SMS_URL'];
        //SMS LANE
        // $this->baseUrl = $this->url."/vendorsms/pushsms.aspx?";
        // $this->baseUrl .= "user=".$this->username;
        // $this->baseUrl .= "&password=".$this->password;
        // $this->baseUrl .= "&sid=".$this->senderID;
        // $this->baseUrl .= "&msisdn=91";

        //msg91
        $this->baseUrl = $this->url."?";
        $this->baseUrl .= "&authkey=".$this->password;
        $this->baseUrl .= "&sender=".$this->senderID;
        $this->baseUrl .= "&route=".$this->route;
        $this->baseUrl .= "&country=91";
        $this->baseUrl .= "&mobiles=";
    }

    /**
     *   Send SMS
     *
     * @param string $url URL
     *
     * @return json
     */
    private function _send($url = null)
    {
        if ($url != null) {
            $http = new Client();
            $response = $http->get($url);

            return $response->json;
        }
    }

    /**
     *   Send Order Confirmation SMS
     *
     * @param array $order Order Data
     *
     * @return json
     */
    public function sendOrderConfirmation($order = [])
    {
        $template = $this->baseUrl;
        $template .= $order['mobile'];
        $template .= "&message=We have received your order ";
        $template .= $order['code']." amounting to Rs. ";
        $template .= $order['grand_total'];
        $template .= " and it is being processed. ";
        $template .= "Check your email for more details. ";
        $template .= "Regards, CakeStudio Team";

        $response = $this->_send($template);
        return $response;
    }

    /**
     *   Send Reset Password SMS
     *
     * @param array $user User Data
     *
     * @return json
     */
    public function sendResetPassword($user = [])
    {
        $template = $this->baseUrl;
        $template .= $user['mobile'];
        $template .= "&message=We heard that you forgot your password.";
        $template .= " Please reset password using this link ".$user['link'];
        $template .= " . ";
        $template .= "Regards, CakeStudio Team";
        $response = $this->_send($template);
        return $response;
    }

}
?>
