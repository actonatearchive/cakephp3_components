

- AppController:

`
try {
    $this->loadComponent('RememberMe');
    if(!empty($this->Auth->user()))  {
        $this->set('userInfo',$this->Auth->user());
        $this->user_id = $this->Auth->user('id');
    } else {
        $check = $this->RememberMe->getUserData();
        if ($check != false) {
          //ReLoggedIn Him
          $this->Auth->setUser($check);
          $this->set('userInfo',$this->Auth->user());
          $this->user_id = $this->Auth->user('id');
        } else {
          $this->set('userInfo',NULL);
          $this->user_id = null;
        }
    }
  }
  catch(Exception $err)
  {
      echo $err;
  }
`

- Login / Signup Action:
- After Successful LoggedIn/Registered
//Save RememberMe Cookie
$this->RememberMe->setUserData($user);

- Logout Action:
//Remove cookie after logout.
$this->RememberMe->removeUserData();
