<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   SQS
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Aws\Sqs\SqsClient;
use Aws\Credentials\Credentials;
use Cake\Core\Configure;

use Cake\ORM\TableRegistry;

/**
 * SQS Component
 *
 * @category Component
 * @package  SQS
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.actonate.com/
 */
class SQSComponent extends Component
{
    public $components = ['Special'];

    /**
     *  Connect to SQS
     *    DATE: 17th March 2017
     *
     * @param string $queueLocation Queue location
     *
     * @return array
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _connect($queueLocation)
    {
        $config = $this->_getAwsConfig();
        //Verify Credentials

        if ($queueLocation === 0) {
            return SqsClient::factory(
                array(
                'region' => $config['region'],
                'version' => $config['version'],
                'http' => [
                    'verify' => false
                    ]
                )
            );
        } else {
            // code...
            return SqsClient::factory(
                array(
                'region' => $config['region2'],
                'version' => $config['version'],
                'http' => [
                    'verify' => false
                    ]
                )
            );
        }
    }


    /**
     *  Get AWS Configs
     *    DATE: 17th March 2017
     *
     * @return array
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _getAwsConfig()
    {
        return Configure::read('aws');
    }


    /**
     *  Get Queues Configs
     *    DATE: 17th March 2017
     *
     * @return array
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _getQueueConfig()
    {
        return Configure::read('queues');
    }


    /**
     *  Get SendOptions Configs
     *    DATE: 17th March 2017
     *
     * @return array
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _getSendOptionConfig()
    {
        return Configure::read('sendOptions');
    }


    /**
     *   Send Message
     *
     * @param string  $queueName     Queue Name
     * @param string  $task          Task Name
     * @param string  $entity        Array To push
     * @param string  $action        Array To push
     * @param array   $payload       Array Payload
     * @param integer $queueType     Queue Type (Standard or FIFO)
     * @param integer $queueLocation Queue Location (ap-south-east or oregon)
     *
     * @return array
     */
    public function sendMessage($queueName,$task ,$entity, $action ,
        $payload,$queueType = 0,$queueLocation = 0
    ) {
        $config = $this->_getQueueConfig();
        $sendOption = $this->_getSendOptionConfig();
        // echo "JAJAJJA";
        // print_r( $config);
        // die();
        $mainArr = array(
            'task' => $task,
            'entity' => $entity,
            'action' => $action,
            'payload' => $payload
        );

        if ($queueType === 0) {
            //Standard Queue
            $result = $this->_connect($queueLocation)->sendMessage(
                array(
                'QueueUrl'     => $config[$queueName]['url'],
                'MessageBody'  => json_encode($mainArr),
                'DelaySeconds' => $sendOption['delay_sec'],
                //ONly For Standard Queue disable it for FIFO
                )
            );
        } else {
            //FIFO Queue
            $result = $this->_connect($queueLocation)->sendMessage(
                array(
                'QueueUrl'     => $config[$queueName]['url'],
                'MessageBody'  => json_encode($mainArr),
                'MessageGroupId' => $payload['id'] //FIFO
                )
            );
        }
        return $result->toArray();
    }


    /**
     *   Send Batch Messages
     *
     * @param string  $queueName     Queue Name
     * @param string  $arrData       Array To push
     * @param integer $queueType     Queue Type (Standard or FIFO)
     * @param integer $queueLocation Queue Location (ap-south-east or oregon)
     *
     * @return array
     */
    public function sendBatchMessage($queueName,$arrData,$queueType = 0,
        $queueLocation = 0
    ) {
        $config = $this->_getQueueConfig();
        $sendOption = $this->_getSendOptionConfig();

        $entries = array();

        for ($idx = 0 ; $idx < sizeof($arrData); $idx++) {
            if ($queueType === 0) {
                if (isset($arrData[$idx]['payload']['id'])) {
                    $entries[] = array(
                        'Id' => $arrData[$idx]['payload']['id'],
                        'MessageBody' => json_encode($arrData[$idx]),
                        'DelaySeconds' => $sendOption['delay_sec']
                    );
                } else {
                    // code...
                    $entries[] = array(
                        'Id' => $this->Special->UUIDv4(),
                        'MessageBody' => json_encode($arrData[$idx]),
                        'DelaySeconds' => $sendOption['delay_sec']
                    );
                }
            } else {
                if (isset($arrData[$idx]['payload']['id'])) {
                    $entries[] = array(
                        'Id' => $arrData[$idx]['payload']['id'],
                        'MessageBody' => json_encode($arrData[$idx]),
                        'MessageGroupId' => $arrData[$idx]['payload']['id']
                    );
                } else {
                    // code...
                    $uuid = $this->Special->UUIDv4();
                    $entries[] = array(
                        'Id' => $uuid,
                        'MessageBody' => json_encode($arrData[$idx]),
                        'MessageGroupId' => $uuid
                    );
                }
            }
        }
        $result = $this->_connect($queueLocation)->sendMessageBatch(
            array(
            'QueueUrl' => $config[$queueName]['url'],
            'Entries' => $entries
            )
        );
        return $result->toArray();
    }
}
?>
