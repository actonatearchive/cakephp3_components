<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   RememberMe
 * @author    Mohammed Sufyan Shaikh <sufyan297@gmail.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. LTD.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


//Imports
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

/**
 * RememberMe Component
 *
 * @category Component
 * @package  RememberMe
 * @author   Mohammed Sufyan Shaikh <sufyan297@gmail.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.actonate.com/
 */

class RememberMeComponent extends Component
{
    public $components = ['Special','User'];

    /**
    *  Set User Data
    *    DATE: 5th September 2017
    *
    * @param array $data User Data (Auth)
    *
    * @return Array Order
    * @author Mohammed Sufyan <sufyan297@gmail.com>
    */
    public function setUserData($data = null)
    {
        return $this->Special->setCookie('RememberMe', $data);
    }

    /**
    *  Get User Data
    *   Date: 5th September 2017
    *
    * @return Array order
    * @author Mohammed Sufyan <sufyan297@gmail.com>
    */
    public function getUserData()
    {
        if ($this->Special->isCookieExists('RememberMe')) {
            $data = $this->Special->getCookie('RememberMe');

            if (isset($data['username'])) {
                if ($this->User->userExists($data['username'])) {
                    //Yes This is Correct User
                    //Return His Login Data
                    $userData = $this->User->getUserDataByUsername(
                        $data['username'],
                        [
                            'id',
                            'username',
                            'user_no',
                            'email',
                            'first_name',
                            'last_name',
                            'news_letters',
                            'verify_token',
                            'is_verified',
                            'del_flag',
                            'organization_id',
                            'forgot_token',
                            'created',
                            'modified'
                        ]
                    );
                    return $userData;
                } else {
                    //Invalid Username
                    return false;
                }
            } else {
                //Cookie Hijacked
                return false;
            }
        } else {
            //Cookie Does not Exists.
            return false;
        }
    }

    /**
    *   Delete Cookie on Manual Logout
    *
    * @return true
    */
    public function removeUserData()
    {
        if ($this->Special->isCookieExists('RememberMe')) {
            $this->Special->deleteCookie('RememberMe');
        }
        return true;
    }
}