<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   Cart
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

/**
 * Cart Component
 *
 * @category Component
 * @package  Cart
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.actonate.com/
 */

class CartComponent extends Component
{
    public $components = ['Cookie','Special','Item','ShavePlan'];
    /**
     * Create Cart Method
     *
     * @return void
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function createCart()
    {
        $cartItems = array();
        if (!$this->Special->checkSession('CartItems')) {
            // $this->Cookie->configKey('CartItems', 'path', '/');

            // $this->Cookie->configKey('CartItems', 'encryption', true);
            // $this->Cookie->write('CartItems', $cartItems);
            $this->setCookie('CartItems', $cartItems);

        }

        return $this->getCookie('CartItems');
    }


    /**
     * Get User Cart
     *
     * @param uuid $user_id User ID
     *
     * @return array cartItems
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getUserCart($user_id = null)
    {
        if($user_id === null) {
            //Null getFromCookies
            return $this->getCartItemsFromCookies();
        } else {
            //getFrom Database
            return $this->getCartItemsFromDB($user_id);
        }
    }


    /**
     * Get Cart Items From Table `axi_cart_items`
     *
     * @param uuid $user_id User ID
     *
     * @return array cartItems
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getCartItemsFromDB($user_id = null)
    {
        //Model->TableName
        $cartitems = TableRegistry::get('CartItems');

        $resultsArray = array();


        $resultsArray = $cartitems
            ->findByUserId($user_id)
            ->contain('Items')
            ->toArray();

        $temp = array();
        $result = array();
        foreach ($resultsArray as $val) {
            $temp['item_id'] = $val['item']['id'];
            $temp['item_name'] = $val['item']['name'];
            $temp['qty'] = $val['quantity'];
            $temp['price'] = $val['item']['price'];
            $temp['discount_price'] = $val['item']['discount_price'];
            $temp['item_image_url'] = $this->Special->getBaseImageUrl()
            ."item/primary_photo/".$val['item']['primary_photo_directory']
            ."/"
            .$val['item']['primary_photo'];

            $temp['small_item_image_url'] = $this->Special->getBaseImageUrl()
            ."item/primary_photo/".$val['item']['primary_photo_directory']
            ."/small_"
            .$val['item']['primary_photo'];

            $temp['one_line3'] = $val['item']['one_line3'];
            $temp['subscription_enabled'] = $val['item']['subscription_enabled'];
            array_push($result, $temp);
        }
        $result = $this->Special->sortMultiArray('item_name', $result, "ASC");
        return $result;
    }


    /**
     * Get Cart Items From Cookies CartItems
     *
     * @return array cartItems
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getCartItemsFromCookies()
    {
        //Model->TableName
        $items = TableRegistry::get('Items');

        $item_cookies = $this->getCookie('CartItems');
        //pr($item_cookies);die('ok');
        if (empty($item_cookies)) {
            return false;
        }
        $item_ids = array();
        foreach ($item_cookies as $val) {
            array_push($item_ids, $val['item_id']);
        }

        $tmp = $items->find('all')
            ->where(['id IN' => $item_ids])
            ->toArray();

        return $this->_resultArray($tmp, $item_cookies);
    }


    /**
     *   Result of CartItems in proper manner
     *
     * @param array $arr1 items table array
     * @param array $arr2 cookies array
     *
     * @return array Result Array of CartItems
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _resultArray($arr1 = array(), $arr2 = array())
    {
        $temp = array();
        $result = array();
        foreach ($arr1 as $val) {
            foreach ($arr2 as $val2) {
                if ($val2['item_id'] == $val['id']) {

                    $temp['item_id'] = $val['id'];
                    $temp['item_name'] = $val['name'];
                    $temp['qty'] = $val2['qty'];
                    $temp['price'] = $val['price'];
                    $temp['discount_price'] = $val['discount_price'];
                    $temp['item_image_url'] = $this->Special->getBaseImageUrl()
                    ."item/primary_photo/"
                    .$val['primary_photo_directory']
                    ."/"
                    .$val['primary_photo'];

                    $temp['small_item_image_url'] = $this->Special->getBaseImageUrl()
                    ."item/primary_photo/".$val['primary_photo_directory']
                    ."/small_"
                    .$val['primary_photo'];

                    $temp['one_line3'] = $val['one_line3'];
                    $temp['subscription_enabled'] = $val['subscription_enabled'];

                    array_push($result, $temp);
                }
            }
        }

        $result = $this->Special->sortMultiArray('item_name', $result, "ASC");
        return $result;
    }


    /**
     *   Sync Cart Items with Table axi_cart_items
     *       $user_id is compulsory required.
     *
     * @param uuid $user_id User ID
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function syncCartItemsWithDB($user_id = null)
    {
        if ($user_id === null) {
            return false;
        }

        //Let's Merge Table Data and Cookie Array (Unique Values)
        //Don't Pass UserID to Get Cookies
        $arr_cookies = $this->getUserCart();
        //Pass UserID to Get TableData
        $arr_data = $this->getUserCart($user_id);
        $arr_tmp = array(); //Temp Array to merge
        $temp = array();

        //First Get Cookies Items
        if (!empty($arr_cookies)) {
            foreach ($arr_cookies as $val) {
                $temp['item_id'] = $val['item_id'];
                $temp['qty'] = $val['qty'];
                array_push($arr_tmp, $temp);
            }
        }
        //Second Get Table Data Items
        foreach ($arr_data as $val) {
            $temp['item_id'] = $val['item_id'];
            $temp['qty'] = $val['qty'];
            array_push($arr_tmp, $temp);
        }

        //Now we got Both Merged.
        //Let's Find Unique Values Only.
        $result = $this->Special->uniqueArray($arr_tmp, 'item_id');

        //Also Add same array in CartItems cookie.
        // $this->Cookie->write('CartItems', $result);
        $this->setCookie('CartItems', $result);
        return $this->insertCartItems($user_id, $result);
    }


    /**
     *   Insert Cart Items in Table: axi_cart_items
     *
     * @param uuid  $user_id   User ID
     * @param array $arr_items Unique Cart Items
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function insertCartItems($user_id = null, $arr_items = array())
    {
        //First Remove User ID Cart Items From axi_cart_items
        //Then Insert New Items

        //Model->TableName
        $cartitems = TableRegistry::get('CartItems');

        $cartitems->deleteAll(
            [
            'user_id' => $user_id
            ]
        );

        //Existing Items Remove Now Let's Insert New Cart Items
        $crtItems = array();
        foreach ($arr_items as $val) {
            $tmp = array();
            $tmp['item_id'] = $val['item_id'];
            $tmp['user_id'] = $user_id;
            $tmp['quantity'] = $val['qty'];
            array_push($crtItems, $tmp);
        }
        $entities = $cartitems->newEntities($crtItems);
        if ($result = $cartitems->saveMany($entities)) {
            return true;
        }

        return false;
    }


    /**
     *   Remove Cart Item From Table axi_cart_items
     *
     * @param uuid $item_id Item ID
     * @param uuid $user_id User ID
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _removeCartItem($item_id = null, $user_id = null)
    {
        $cartitems = TableRegistry::get('CartItems');

        if ($item_id != null && $user_id != null) {
            $cartitems->deleteAll(
                [
                'user_id' => $user_id,
                'item_id' => $item_id
                ]
            );

            return true;
        }
        return false;
    }


    /**
     * Get Cookie
     *
     * @param string $key Key To Access Cookie
     *
     * @return cookie value
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getCookie($key = null)
    {
        //return $this->Cookie->read($key);
        return $this->Special->getSession($key);
    }


    /**
     *   Set Cookie Wrapper
     *
     * @param string $key   Key For Cookie
     * @param string $value Value For Cookie
     *
     * @return cookie value
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function setCookie($key = null, $value = null)
    {
        // $this->Cookie->config(
        //     [
        //     'expires' => '+180 days',
        //     'httpOnly' => false
        //     ]
        // );
        //
        // return $this->Cookie->write($key, $value);
        //
        //
        // fix
         $this->Special->setSession($key, $value);
         //pr($this->Special->getSession($key));die();
         return $this->Special->getSession($key);
    }


    /**
     * Remove Cookie
     *
     * @param string $key Key To Remove Cookie
     *
     * @return cookie value
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function deleteCookie($key = null)
    {
        if ($key != null) {
            //return $this->Cookie->delete($key);
            return $this->Special->deleteSession($key);
        }
        return false;
    }


    /**
     * Delete Cart Method
     *
     * @param uuid $user_id User ID
     *
     * @return void
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function deleteCart($user_id = null)
    {
        if ($user_id != null) {
            $cartitems = TableRegistry::get('CartItems');

            // CartItems
            if ($this->Special->checkSession('CartItems')) {
                $this->deleteCookie('CartItems');
            }

            //Remove User Cart Items From Db
            $cartitems->deleteAll(
                [
                'user_id' => $user_id
                ]
            );

            return true;
        } else {
            // CartItems
            if ($this->Special->checkSession('CartItems')) {
                $this->deleteCookie('CartItems');
            }
        }
        return false;
    }


    /**
     * Check Stock Method
     *
     * @param string  $item_id      ItemID
     * @param integer $purchase_qty quantity
     *
     * @return void
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function checkStock($item_id='',$purchase_qty=1)
    {
    }


    /**
     * Add Items To Cart Method
     * This will also create cart if it is not exists
     *
     * @param uuid    $item_id ItemID
     * @param integer $qty     Item Quantity
     * @param uuid    $user_id User ID
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function addItem($item_id = null, $qty = 1, $user_id = null)
    {
        $cartItems = array();
        $tmp_item = $this->Item->getItemData(
            $item_id, ['is_active','out_of_stock']
        );
        if ($tmp_item == null) {
            return false;
        }
        if ($tmp_item['is_active'] != 1 || $tmp_item['out_of_stock'] != 0 ) {
            return false;
        }
        $cartItems = $this->createCart();

        if (sizeof($cartItems) < $this->getCartLimit()) {
            $tmp['item_id'] = $item_id;
            $chk = false;

            if (!empty($cartItems)) {
                $chk = $this->_checkItemsQty($cartItems, $item_id, $qty);
            }
            if ($chk == false) {
                $tmp['qty'] = $qty;
                array_push($cartItems, $tmp);
            } else {
                $cartItems = $chk; //Updated Array Items
            }

            // $this->Cookie->write('CartItems', $cartItems);
            $this->setCookie('CartItems', $cartItems);

            //If User is logged In Then This will be called.
            if ($user_id != null) {
                return $this->syncCartItemsWithDB($user_id);
            }

            return true;
        } else {
            return false;
        }
    }


    /**
     *   Get Cart Item By ID
     *
     * @param uuid    $item_id ItemID
     * @param uuid    $user_id User ID
     * @param integer $qty     Quantity of item
     *
     * @return array / boolean
     *
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getCartItemById($item_id = null,$user_id = null,$qty = 1)
    {
        $result = array();
        $tmp = $this->getUserCart($user_id);
        //pr($tmp);die();

        $temp = array();
        if (!empty($tmp)) {
            $i = 0;
            foreach ($tmp as $val1) {
                $temp[$i]['qty'] = $val1['qty'];
                $temp[$i]['discount_price'] = $val1['discount_price'];
                $temp[$i]['item_id'] = $val1['item_id'];
                $i++;
            }
        }
        if ($item_id != null) {
            $i = 0;
            if (!empty($tmp)) {
                foreach ($tmp as $key => $value) {
                    if ($value['item_id'] == $item_id) {
                        $temp[$i]['qty'] = ($value['qty'] + $qty);
                        $temp[$i]['discount_price'] = $value['discount_price'];
                        $result['temp_qty'] = ($value['qty'] + $qty);
                        $result['qty'] = $value['qty'];
                        $result['discount_price'] = $value['discount_price'];
                        break;
                    }
                    $i++;
                }
            }
        }

        $result['sub_total'] = 0;
        $result['discount_price'] = 0;
        foreach ($temp as $val) {
            if (empty($result['temp_qty']) || !isset($result['temp_qty'])) {
                $result['temp_qty'] = 1;
            }

            if ($val['discount_price'] === 0 || empty($val['discount_price'])) {
                continue;
            }
            $result['sub_total'] += ($val['qty'] * $val['discount_price']);
            if ($item_id == $val['item_id']) {
                $result['discount_price'] = $val['discount_price'];
            }
        }
        return $result;
    }

    /**
     *  Check whether that item is already exists
     *  If it is already exists then Increase it's Qty.
     *
     * @param array   $arr_items Array Items
     * @param uuid    $item_id   Item ID
     * @param integer $qty       Quantity.
     *
     * @return integer
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _checkItemsQty($arr_items = array(), $item_id = null, $qty = 1)
    {
        $i = 0;
        foreach ($arr_items as $val) {
            if ($val['item_id'] == $item_id) {
                $arr_items[$i]['qty'] = $val['qty'] + $qty;
                return $arr_items;
            }
            $i++;
        }
        return false;
    }


    /**
     *   Delete Item From Cart
     *
     * @param uuid $item_id Item ID
     * @param uuid $user_id User ID - Optional
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function deleteItem($item_id = null, $user_id = null)
    {
        $cartItems = array();

        //getCurrent Items From Cart
        if ($user_id != null) {
            $cartItems = $this->getUserCart($user_id);
        } else {
            $cartItems = $this->getUserCart();
        }

        $newCartItems = array();
        foreach ($cartItems as $val) {
            if ($item_id != $val['item_id']) {
                $newCartItems[] = $val;
            }
        }
        // $this->Cookie->write('CartItems', $newCartItems);
        $this->setCookie('CartItems', $newCartItems);

        //If User is logged In Then This will be called.
        if ($user_id != null) {
            $this->_removeCartItem($item_id, $user_id);
            return $this->syncCartItemsWithDB($user_id);
        }
        return false;
    }


    /**
     *   No. of Items in Cart
     *
     * @param uuid $user_id User ID
     *
     * @return integer
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getCartItemCount($user_id = null)
    {
        if ($user_id == null) {
            $tmp = $this->getUserCart($user_id);
            if (!empty($tmp)) {
                return sizeof($tmp);
            } else {
                return 0;
            }
        } else {
            return sizeof($this->getUserCart($user_id));
        }
    }


    /**
     *   Maximum No. of Items Cart Limit
     *
     * @return integer
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getCartLimit()
    {
        $tmp = $this->Special->getConfigs('global');
        if (isset($tmp['cart_items_limit'])) {
            return $tmp['cart_items_limit'];
        } else {
            return 15;
        }
    }

    /**
     *   Check Total if Total is Less than shipping_below amount
     *   Then add Shipping Charges
     *
     * @param uuid    $item_id   Item ID
     * @param integer $sub_total Total of Items
     *
     * @return array
     */
    public function checkShippingCharges($item_id = null, $sub_total = 0)
    {

        $config = $this->Special->getConfigs('shipping');
        $item = $this->Item->getItemData($item_id, ['item_shipping_charges']);

        $resultsArray = array();

        if ($item['item_shipping_charges'] == 0
            || $item['item_shipping_charges'] === 0
        ) {
            //Don't Apply Shipping Charges
            $resultsArray['shipping_charges'] = 0;
            $resultsArray['sub_total'] = $sub_total;

        } else {
            if ($sub_total < $config['shipping_below']) {
                // $sub_total += $config['shipping_charges'];
                $resultsArray['shipping_charges'] = $config['shipping_charges'];
            } else {
                $resultsArray['shipping_charges'] = 0;
            }

            $resultsArray['sub_total'] = $sub_total;
        }
        $resultsArray['shipping_below'] = $config['shipping_below'];

        return $resultsArray;
    }


    /**
     *   Something Went Wrong (Reset Cart)
     *
     * @param uuid    $user_id Item ID
     * @param boolean $flag    delete subscription details
     *
     * @return boolean
     */
    public function resetCart($user_id = null,$flag = 0)
    {
        //'Sorry some error occurred! Please again add cart items.'
        if ($user_id != null) {
            $this->deleteCart($user_id);
        }
        $this->deleteCart();
        $this->Special->deleteCookie('cart_config');//To Show Normal Cart
        if ($flag == 0) {
            $this->ShavePlan->deleteSubscriptionDetails();
        }

        return true;
    }
}


?>
