<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   SES
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;
namespace App\Mailer\Transport;



use Cake\Controller\Component;
use Aws\Credentials\Credentials;
use Aws\Ses\SesClient;
use Cake\Core\Configure;
use Cake\Mailer\AbstractTransport;
use Cake\Mailer\Email;

/**
 * SES Component
 *  We are no more using This Component
 *
 * @category Component
 * @package  SES
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.actonate.com/
 */
class SESComponent extends Component extends AbstractTransport
{
    public $components = ['Special'];

    /**
     * Connect to SES
     *
     * @return array
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _connect()
    {
        $config = $this->_getAwsConfig();

        return $client = SesClient::factory(
            array(
            'version'=> $config['version'],
            'region' => $config['region3'],
            'http' => [
                'verify' => false
                ]
            )
        );
    }

    /**
     *  Get AWS Configs
     *    DATE: 17th March 2017
     *
     * @return array
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _getAwsConfig()
    {
        return Configure::read('aws');
    }


    /**
     *   Send Email from AWS
     *
     * @param string $to      To   whom (email address)
     * @param string $subject Subject of Email
     * @param string $message Message Body of Email
     *
     * @return $messageId
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function sendMail($to = null, $subject = null, $message = null)
    {
        $request = array();
        $request['Source'] = 'noreply@letsshave.com';
        $request['Destination']['ToAddresses'] = array($to);
        $request['Message']['Subject']['Data'] = $subject;
        $request['Message']['Body']['Text']['Data'] = $message;

        $result = array();
        try {
            $result = $this->_connect()->sendEmail($request);
            return $result->get('MessageId');
        } catch (Exception $e) {
            $this->log("The email was not sent. Error message: ");
            $this->log($e->getMessage());
        }
    }


    /**
     *   Send Email from AWS
     *
     * @param string $from    From which Email Address
     * @param string $to      To   whom (email address)
     * @param string $subject Subject of Email
     * @param string $message Message Body of Email
     *
     * @return $messageId
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function sendMailFrom($from = null, $to = null, $subject = null,
        $message = null
    ) {
        $request = array();
        $request['Source'] = $from;
        $request['Destination']['ToAddresses'] = array($to);
        $request['Message']['Subject']['Data'] = $subject;
        $request['Message']['Body']['Text']['Data'] = $message;

        try {
            $result = $this->_connect()->sendEmail($request);
            return $result->get('MessageId');
        } catch (Exception $e) {
            $this->log("The email was not sent. Error message: ");
            $this->log($e->getMessage());
        }
    }
}
?>
